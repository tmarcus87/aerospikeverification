package org.clubdude.tmarcus.asverify;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Key;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.Statement;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.clubdude.tmarcus.asverify.util.PrefixCounter;

import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * @author ono_takahiko
 * @since 2015/01/07
 */
@Slf4j
public class Verification {

	private static final int NUM_OF_THREADS = 30;

	private static final String HOST = "127.0.0.1";
	private static final int PORT = 3000;

	private static final String NS = "test";

	private static final int NUM_OF_KEYS = 1 * 1000 * 1000;
	private static final int SIZE_OF_DATA = 255;

	private ExecutorService executor = Executors.newFixedThreadPool(NUM_OF_THREADS);

	private PrefixCounter keyCounter = new PrefixCounter("key");

	public void clean() throws Exception {
		AerospikeClient client = new AerospikeClient(HOST, PORT);
		Statement statement = new Statement();
		statement.setNamespace(NS);
		RecordSet recordSet = client.query(null, statement);
		while (recordSet.next()) {
			Key key = recordSet.getKey();
			client.delete(null, key);
			log.trace("Delete key({})", key);
		}
		log.info("Finished clean");
		client.close();
	}

	public void start() throws Exception {
		log.info("Start");
		long start = System.currentTimeMillis();
		List<Key> keys = Lists.newArrayListWithCapacity(NUM_OF_KEYS);

		for (int i = 0; i < NUM_OF_KEYS; i++) {
			Key key = new Key(NS, null, keyCounter.getAndIncrement());
			keys.add(key);
			executor.execute(new Task(key, SIZE_OF_DATA, HOST, PORT));
		}

		executor.shutdown();
		while(!executor.isTerminated()) {
			Thread.sleep(500L);
		}
		log.info("Finished (TAT:{}ms)",
				String.format("%1$,3d", System.currentTimeMillis() - start));
	}

}
