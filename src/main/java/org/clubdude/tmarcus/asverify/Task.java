package org.clubdude.tmarcus.asverify;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.google.common.collect.Iterables;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.clubdude.tmarcus.asverify.util.PrefixCounter;

import java.util.List;

/**
 * @author ono_takahiko
 * @since 2015/01/07
 */
@Slf4j
public class Task implements Runnable {

	private static final int NUM_OF_BINS = 100;
	private static final char CHAR_OF_DATA = 'x';

	private int sizeOfData;

	private Key key;

	private AerospikeClient aerospikeClient;

	private PrefixCounter binCounter = new PrefixCounter("bin");

	public Task(Key key, int sizeOfData, String host, int port) {
		this.key = key;
		this.sizeOfData = sizeOfData;
		this.aerospikeClient = new AerospikeClient(host, port);
		log.trace("Connected / {}:{}", host, port);
	}

	@Override
	public void run() {
		List<Bin> bins = Lists.newArrayList();
		for (int i = 0; i < NUM_OF_BINS; i++) {
			bins.add(new Bin(binCounter.getAndIncrement(), getData()));
		}

		long start = System.currentTimeMillis();
		aerospikeClient.put(null, key, Iterables.toArray(bins, Bin.class));
		log.debug("Finish task({}) with {}ms", key, System.currentTimeMillis() - start);
		aerospikeClient.close();
	}

	private String getData() {
		StringBuffer sb = new StringBuffer(sizeOfData);
		for (int i = 0; i < sizeOfData; i++) {
			sb.append(CHAR_OF_DATA);
		}
		return sb.toString();
	}

}
