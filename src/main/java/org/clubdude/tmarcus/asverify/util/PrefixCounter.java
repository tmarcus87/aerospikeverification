package org.clubdude.tmarcus.asverify.util;

import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ono_takahiko
 * @since 2015/01/08
 */
public class PrefixCounter {

	private String key;

	private String format;

	private AtomicInteger counter = new AtomicInteger(0);

	public PrefixCounter(String key) {
		this(key, "%07d");
	}

	public PrefixCounter(String key, String format) {
		this.key = key;
		this.format = format;
	}

	public String getAndIncrement() {
		return format(counter.getAndIncrement());
	}

	public String incrementAndGet() {
		return format(counter.incrementAndGet());
	}

	public String format(int number) {
		return String.format(
				new StringBuffer(key.length() + format.length())
						.append(key)
						.append(format)
						.toString(),
				number);
	}


}
