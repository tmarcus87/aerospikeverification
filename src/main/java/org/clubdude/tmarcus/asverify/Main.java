package org.clubdude.tmarcus.asverify;

import com.google.inject.Guice;
import com.google.inject.Injector;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;

/**
 * Created by t_marcus87 on 2015/01/07.
 */
@Slf4j
public class Main {

	public static void main(String... args) {
		Injector injector = Guice.createInjector();
		Verification verification = injector.getInstance(Verification.class);
		try {
			verification.clean();
			verification.start();
		} catch (Exception e) {
			log.warn(ExceptionUtils.getStackTrace(e));
		}
	}

}
